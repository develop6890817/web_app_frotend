const express = require('express');
var request = require('request');
var axios= require('axios')
const app = express();
const port = 3000;


app.use(express.urlencoded({extended:false}));
app.use(express.json());

const dotenv = require('dotenv');
dotenv.config({path:'./env/.env'});


app.use("/resources",express.static('public'));
app.use("/resources",express.static(__dirname + '/public'));

app.set('view engine','ejs');

const session = require('express-session');
app.use(session({
    secret:'secret',
    resave: true,
    saveUninitialized:true
}));

app.get('/index',async (req, res) => {
    if (req.session.loggedin){
        token = `Bearer ${req.session.secret}`;
        mdm_ubicacion = await peticion_mdm_ubicacion(token);
        ctx_genero_literario = await peticion_ctx_genero_literario(token);
        fact_libro = await peticion_fact_libro(token);
        mdm_editorial = await peticion_mdm_editorial(token);
        mdm_autor = await peticion_mdm_autor(token);
        mdm_tiempo = await peticion_mdm_tiempo(token);
        relacionautoresLibrosT= relacionarAutoresLibrosTerritorios(JSON.parse(mdm_autor),JSON.parse(fact_libro),JSON.parse(mdm_ubicacion));
        const cantidadLibrosPorNacionalidad = relacionautoresLibrosT.reduce((acumulador, elemento) => {
        const { nacionalidad } = elemento;
        acumulador[nacionalidad] = (acumulador[nacionalidad] || 0) + 1;
        return acumulador;
        }, {});
        const gananciasPorEditorial = relacioneditorialLibros(JSON.parse(fact_libro),JSON.parse(mdm_editorial));
        res.render('index',{
            mdm_ubicacion:JSON.parse(mdm_ubicacion),
            ctx_genero_literario:JSON.parse(ctx_genero_literario),
            fact_libro:JSON.parse(fact_libro),
            mdm_editorial:JSON.parse(mdm_editorial),
            mdm_autor:JSON.parse(mdm_autor),
            mdm_tiempo:JSON.parse(mdm_tiempo),
            cantidadLibrosPorNacionalidad:cantidadLibrosPorNacionalidad,
            gananciasPorEditorial:gananciasPorEditorial});
    }else{res.end();}
});

function relacionarAutoresLibrosTerritorios(autores, libros, territorio) {
    return autores.map(autor => {
      const libro = libros.find(lib => lib.fk_autor === autor.id);
      const terr = territorio.find(terr => terr.id === autor.fk_nacionalidad);
      return {
        id_autor: autor.id,
        nombre_autor: autor.nombre,
        nacionalidad: terr.territorio,
        libro: {
          id_libro: libro.id,
          titulo_libro: libro.titulo
        }
      };
    });
  }
  
function relacioneditorialLibros(libros, editorial){
    var gananciasEditorial = {};
    libros.forEach(libro => {
        var nombreEditorial = editorial.find(ed => ed.id === libro.fk_editorial).nombre_edictorial;
        var precio = libro.precio;
        var cantidad = libro.cantidad;
        var gananciasLibro = precio * cantidad;
      
        if (!gananciasEditorial[nombreEditorial]) {
          gananciasEditorial[nombreEditorial] = 0;
        }
        gananciasEditorial[nombreEditorial] += gananciasLibro;
      });

    return gananciasEditorial;
}


app.get('/logout', (req, res)=>{
    req.session.destroy(()=>{
        req.session = null;
        res.redirect('/login');
    });
})

async function peticion_mdm_tiempo(token){
    let dato;
    item = '/recurso-mdm_tiempo';
   
        var config = {
            method: 'get',
            url: `http://127.0.0.1:5000${item}`,
            headers: { 
              'Authorization': token
            }
          };
          
          await axios(config)
          .then(function (response) {
            dato = response.data;
            
          })
          .catch(function (error) {
            console.log(error);
          });
    
    return dato;
}

async function peticion_fact_libro(token){
            let dato;
            item = '/recurso-fact_libro';
                           
            var config = {
                method: 'get',
                url: `http://127.0.0.1:5000${item}`,
                headers: { 
                    'Authorization': token
                }
                };
                                  
                await axios(config)
                    .then(function (response) {
                        dato = response.data;
                                    
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                            
            return dato;
        }
async function peticion_ctx_genero_literario(token){
            let dato;
            item = '/recurso-ctx_genero_literario';
                           
            var config = {
                method: 'get',
                url: `http://127.0.0.1:5000${item}`,
                headers: { 
                    'Authorization': token
                }
                };
                                  
                await axios(config)
                    .then(function (response) {
                        dato = response.data;
                                    
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                            
            return dato;
        }
async function peticion_mdm_ubicacion(token){
    let dato;
    item = '/recurso-mdm_ubicacion';
                   
    var config = {
        method: 'get',
        url: `http://127.0.0.1:5000${item}`,
        headers: { 
            'Authorization': token
        }
        };
                          
        await axios(config)
            .then(function (response) {
                dato = response.data;
                            
            })
            .catch(function (error) {
                console.log(error);
            });
                    
    return dato;
}

async function peticion_mdm_editorial(token){
    let dato;
    item = '/recurso-mdm_editorial';
           
    var config = {
        method: 'get',
        url: `http://127.0.0.1:5000${item}`,
        headers: { 
            'Authorization': token
        }
        };
                  
        await axios(config)
            .then(function (response) {
                dato = response.data;
                    
            })
            .catch(function (error) {
                console.log(error);
            });
            
    return dato;
    }

async function peticion_mdm_autor(token){
    let dato;
    item = '/recurso-mdm_autor';
           
    var config = {
        method: 'get',
        url: `http://127.0.0.1:5000${item}`,
        headers: { 
            'Authorization': token
        }
        };
                  
        await axios(config)
            .then(function (response) {
                dato = response.data;
                    
            })
            .catch(function (error) {
                console.log(error);
            });
            
    return dato;
    }
async function peticion_mdm_editorial(token){
    let dato;
    item = '/recurso-mdm_editorial';
               
    var config = {
            method: 'get',
            url: `http://127.0.0.1:5000${item}`,
            headers: { 
                'Authorization': token
            }
            };
                      
            await axios(config)
                .then(function (response) {
                    dato = response.data;
                        
                })
                .catch(function (error) {
                    console.log(error);
                });
                
        return dato;
    }

app.get('/login', (req, res) => {
    res.render('login');
    res.end();
  });
  
app.post('/auth', async(req, res)=>{
    const user = req.body.username;
    const pass = req.body.password;
    var options = {
        'method': 'POST',
        'url': 'http://127.0.0.1:5000/login',
        'headers': {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({"user":user,"password":pass})
      
      };
      request(options, function (error, response) {
        if (error) throw new Error(error);
        token = JSON.parse(response.body);
        req.session.loggedin = true;
        req.session.secret = token["token"];
        res.redirect('/index');
        res.end();
      });
})

app.listen(port, (req, res) => {
  console.log(`Servidor escuchando en http://localhost:${port}`);
});
