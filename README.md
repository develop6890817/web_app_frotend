# web_app_frotend



## Inicio del proyecto

El desarrollo web se realizo con la tecnologia de nodejs para realizar un contenido mas ligero


## Integracion y correr el proyecto local se debe:

- [ ] [npm install del package.json para la instalacion de los modulos]
- [ ] [ejecutar el nodemon del archivo app.js para obtener un reload]
- [ ] [actualizar la ip y puerto del servicio backend y tener cuidado con el token]



## Funcionalidades

El proposito del apartado web ser visualizar el contendio de la base de datos analitica para lograr ver las relaciones y el contexto de las tablas:
- tabla maestra: contiene informacion fundamental del negocio y se usa en otros departamentos del mismo
- tabla contexto: solo se usa en una seccion del negocio, propia del mundo
- tabla resumen: facilita la consulta y responder las preguntas mas relevantes del negocio, facilita la adquision de la informacion


## Vistazo rapido Frontend

![Ejemplo1](https://gitlab.com/develop6890817/web_app_frotend/-/raw/main/diagrama_1.JPG?ref_type=heads)
![Ejemplo2](https://gitlab.com/develop6890817/web_app_frotend/-/raw/main/diagrama_2.JPG?ref_type=heads)
![Ejemplo3](https://gitlab.com/develop6890817/web_app_frotend/-/raw/main/diagrama_3.JPG?ref_type=heads)



## Authors and acknowledgment
Jose Manolo Pinzon Hernandez
